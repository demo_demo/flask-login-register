# -*- coding:utf-8 -*-
from flask import render_template,redirect
from . import main
from flask_login import current_user, login_required

@main.route('/')
def index():
     if not current_user.is_authenticated: # 如果登录成败判断
        return redirect('auth/login')
     else:
        return render_template('index.html')


