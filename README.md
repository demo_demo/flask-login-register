#Flask 登录验证、用户注册系统

##### 结合mysql实现了flask的登录验证,用户注册功能


### 初始环境配置
```
yum install -y mysql-devel
cd ~
git clone git@git.oschina.net:demo_demo/flask-login-register.git

pip install virtualenv
virtualenv /usr/local/flaskenv && cd /usr/local/flaskenv/bin/

sed -i "s@https:\/\/pypi.python.org\/@http:\/\/pypi.douban.com\/@g" /usr/local/flaskenv/lib/python2.7/site-packages/pip/models/index.py

./pip install -r ~/flask-login-register/requirements.txt

mysql > grant all on flask.* to flask@'127.0.0.1' identified by 'flask';
mysql > flush privileges; exit;
```

### 启动服务
```
cd ~/flask-login-register/
/usr/local/flaskenv/bin/python manager.py runserver -h YOUR_HOSTIP
```

### 访问：
[http://YOUR_HOSTIP:5000]()

### 示例

##### 登录
![](http://git.oschina.net/demo_demo/flask-login-register/raw/master/screenshots/login.png)

##### 注册
![](http://git.oschina.net/demo_demo/flask-login-register/raw/master/screenshots/register.png)

##### 登录认证后
![](http://git.oschina.net/demo_demo/flask-login-register/raw/master/screenshots/main.png)
